# proyectoPython

Proyecto Python con ejercicios de objestos Json a partir del analisis de la API [pokemon](https://pokeapi.co/)

## Iniciar Proyecto

Antes de iniciar el proeyecto se recoemienda instalr las liberrias del archivo requirements.txt

## Ejecutar poryecto
Para ejecutar el poryecto con las funciiones ceradas en el main, ejecutar el siguiemnte comando:
```
cd {repo_clone}
python main.py
```
***

## Ejecutar Test y reporte de cobertura

Para ejecutar las pruebas unitarias del proyecto con analisis de cobertura, ejecutar el siguiemnte comando:
```
cd {repo_clone}/tests
coverage run -m pytest
```
Para exportar el analsis en HTML, ejecutar el siguiemnte comando:
```
cd {repo_clone}/tests
coverage html
```

El reporte HTML queda generado en al rura repo_clone/tests/htmlcov/index.html

![Cobertura Poryecto](https://i.ibb.co/RQcD5mG/cobertura.png)

