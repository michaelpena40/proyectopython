import json
from ejercicios import punto3

def test_obtain_weight_and_type_pokemons():
    '''Definición de variables'''
    requestPokemon = '{"name":"pokemon-test","types":[{"slot":1,"type":{"name":"psychic","url":"https://pokeapi.co/api/v2/type/14/"}},' \
                     '{"slot":1,"type":{"name":"test","url":"https://pokeapi.co/api/v2/type/14/"}}],"weight":1220}'
    data = json.loads(requestPokemon)
    expectedWeight = 1220
    typePokemon = "psychic"

    '''Ejecución del metodo a probar'''
    result = punto3.obtain_type_and_weigth(data, typePokemon)

    '''Verificación'''
    assert result == expectedWeight

def test_obtain_numbers_weigth_pokemon_season_one_type_psychic():
    '''Definición de variables'''
    url = "https://pokeapi.co/api/v2/pokemon/"
    type = "psychic"
    expectedResult =[195, 565, 480, 360, 785, 324, 756, 25, 1200, 800, 545, 406, 1220, 40]

    '''Ejecución del metodo a probar'''
    result = punto3.obtain_numbers_weigth_pokemon_season_one(url,type)

    '''Verificación'''
    assert expectedResult == result


def test_error_service_weigth_pokemon_season_one():
    expectedResult = "ERROR EN EL SERVICIO"
    url = "https://pokeapi.co/api/v2222/pokemon/"
    type = "psychic"

    '''Ejecución del metodo a probar'''
    result = punto3.obtain_numbers_weigth_pokemon_season_one(url, type)

    '''Verificación'''
    assert expectedResult == result

