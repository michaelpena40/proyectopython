import json
from ejercicios import punto2

def test_obtanin_number_species():
    '''Definición de variables'''
    names = ["pikachu","eve","new"]
    expectedNumber = 3

    '''Ejecución del metodo a probar'''
    result = punto2.obtain_number_species(names)


    '''Verificación'''
    assert result == expectedNumber

def test_obtanin_number_species_not_repeated():
    '''Definición de variables'''
    names = ["pikachu", "pikachu", "new"]
    expectedNumber = 2

    '''Ejecución del metodo a probar'''
    result = punto2.obtain_number_species(names)

    '''Verificación'''
    assert result == expectedNumber

def test_service_obtain_all_names_egg_groups():
    '''Definición de variables'''
    dataEggGroups= '{"base_happiness":50,"capture_rate":75,"egg_groups":[{"name":"ground","url":"https://pokeapi.co/api/v2/egg-group/5/"},{"name":"fairy","url":"https://pokeapi.co/api/v2/egg-group/6/"}]}'
    data = json.loads(dataEggGroups)
    expectedResult = ['rattata', 'raticate', 'ekans', 'arbok', 'pikachu', 'raichu', 'sandshrew', 'sandslash', 'nidoran-f', 'nidoran-m', 'nidorino', 'nidoking', 'vulpix', 'ninetales', 'diglett', 'dugtrio', 'meowth', 'persian', 'psyduck', 'golduck', 'mankey', 'primeape', 'growlithe', 'arcanine', 'ponyta', 'rapidash', 'farfetchd', 'seel', 'dewgong', 'rhyhorn', 'rhydon', 'tauros', 'eevee', 'vaporeon', 'jolteon', 'flareon', 'cyndaquil', 'quilava', 'typhlosion', 'sentret', 'furret', 'mareep', 'flaaffy', 'ampharos', 'aipom', 'wooper', 'quagsire', 'espeon', 'umbreon', 'girafarig', 'dunsparce', 'snubbull', 'granbull', 'sneasel', 'teddiursa', 'ursaring', 'swinub', 'piloswine', 'delibird', 'houndour', 'houndoom', 'phanpy', 'donphan', 'stantler', 'smeargle', 'miltank', 'torchic', 'combusken', 'blaziken', 'poochyena', 'mightyena', 'zigzagoon', 'linoone', 'seedot', 'nuzleaf', 'shiftry', 'slakoth', 'vigoroth', 'slaking', 'whismur', 'loudred', 'exploud', 'skitty', 'delcatty', 'mawile', 'electrike', 'manectric', 'wailmer', 'wailord', 'numel', 'camerupt', 'torkoal', 'spoink', 'grumpig', 'spinda', 'zangoose', 'seviper', 'kecleon', 'absol', 'spheal', 'sealeo', 'walrein', 'chimchar', 'monferno', 'infernape', 'piplup', 'prinplup', 'empoleon', 'bidoof', 'bibarel', 'shinx', 'luxio', 'luxray', 'pachirisu', 'buizel', 'floatzel', 'ambipom', 'buneary', 'lopunny', 'glameow', 'purugly', 'stunky', 'skuntank', 'lucario', 'hippopotas', 'hippowdon', 'weavile', 'rhyperior', 'leafeon', 'glaceon', 'mamoswine', 'snivy', 'servine', 'serperior', 'tepig', 'pignite', 'emboar', 'oshawott', 'dewott', 'samurott', 'patrat', 'watchog', 'lillipup', 'herdier', 'stoutland', 'purrloin', 'liepard', 'pansage', 'simisage', 'pansear', 'simisear', 'panpour', 'simipour', 'munna', 'musharna', 'blitzle', 'zebstrika', 'woobat', 'swoobat', 'drilbur', 'excadrill', 'sandile', 'krokorok', 'krookodile', 'darumaka', 'darmanitan', 'scraggy', 'scrafty', 'zorua', 'zoroark', 'minccino', 'cinccino', 'deerling', 'sawsbuck', 'emolga', 'cubchoo', 'beartic', 'mienfoo', 'mienshao', 'bouffalant', 'heatmor', 'chespin', 'quilladin', 'chesnaught', 'fennekin', 'braixen', 'delphox', 'bunnelby', 'diggersby', 'litleo', 'pyroar', 'skiddo', 'gogoat', 'pancham', 'pangoro', 'furfrou', 'espurr', 'meowstic', 'sylveon', 'dedenne', 'litten', 'torracat', 'incineroar', 'popplio', 'brionne', 'primarina', 'yungoos', 'gumshoos', 'rockruff', 'lycanroc', 'mudbray', 'mudsdale', 'stufful', 'bewear', 'oranguru', 'passimian', 'komala', 'togedemaru', 'grookey', 'thwackey', 'rillaboom', 'scorbunny', 'raboot', 'cinderace', 'sobble', 'drizzile', 'inteleon', 'skwovet', 'greedent', 'nickit', 'thievul', 'wooloo', 'dubwool', 'yamper', 'boltund', 'silicobra', 'sandaconda', 'obstagoon', 'perrserker', 'sirfetchd', 'eiscue', 'morpeko', 'cufant', 'copperajah', 'pikachu', 'raichu', 'clefairy', 'clefable', 'jigglypuff', 'wigglytuff', 'chansey', 'togetic', 'marill', 'azumarill', 'hoppip', 'skiploom', 'jumpluff', 'snubbull', 'granbull', 'blissey', 'shroomish', 'breloom', 'skitty', 'delcatty', 'mawile', 'plusle', 'minun', 'roselia', 'castform', 'snorunt', 'glalie', 'roserade', 'pachirisu', 'cherubi', 'cherrim', 'togekiss', 'froslass', 'phione', 'manaphy', 'audino', 'cottonee', 'whimsicott', 'flabebe', 'floette', 'florges', 'spritzee', 'aromatisse', 'swirlix', 'slurpuff', 'dedenne', 'carbink', 'cutiefly', 'ribombee', 'togedemaru', 'hatenna', 'hattrem', 'hatterene', 'impidimp', 'morgrem', 'grimmsnarl', 'milcery', 'alcremie', 'falinks', 'indeedee', 'morpeko']

    '''Ejecución del metodo a probar'''
    result = punto2.obtain_all_names_egg_groups(data)

    '''Verificación'''
    assert result == expectedResult

def test_error_service_obtain_all_names_egg_groups():
    '''Definición de variables'''
    dataEggGroups= '{"base_happiness":50,"capture_rate":75,"egg_groups":[{"name":"ground","url":"https://pokeapi.co/api/v2/egg-group/5/"},{"name":"fairy","url":"https://pokeapi.co/api/v28888/egg-group/6/"}]}'
    data = json.loads(dataEggGroups)
    expectedResult = "ERROR EN EL SERVICIO"

    '''Ejecución del metodo a probar'''
    result = punto2.obtain_all_names_egg_groups(data)

    '''Verificación'''
    assert result == expectedResult




