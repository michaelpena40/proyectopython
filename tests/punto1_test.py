import json
from ejercicios import punto1

def test_name_pokemons_with_a_and_ar():
    '''Definición de variables'''
    requestPokemons = '{"count":1126,"next":null,"previous":null,"results":[' \
                      '{"name":"bulbasaur","url":"https://pokeapi.co/api/v2/pokemon/1/"},' \
                      '{"name":"test-ata","url":"https://pokeapi.co/api/v2/pokemon/2/"},' \
                      '{"name":"only-at-test","url":"https://pokeapi.co/api/v2/pokemon/2/"},' \
                      '{"name":"venusaur","url":"https://pokeapi.co/api/v2/pokemon/3/"}]}'
    data = json.loads(requestPokemons)
    expexted = 3

    '''Ejecución del metodo a probar'''
    result = punto1.obtain_punto_1(data)

    '''Verificación'''
    assert result == expexted


def test_name_pokemons_not_contains_at_and_a():
    '''Definición de variables'''
    requestPokemons = '{"count":1126,"next":null,"previous":null,"results":[' \
                      '{"name":"pikachu","url":"https://pokeapi.co/api/v2/pokemon/1/"},' \
                      '{"name":"test-not-a","url":"https://pokeapi.co/api/v2/pokemon/2/"},' \
                      '{"name":"doto","url":"https://pokeapi.co/api/v2/pokemon/3/"}]}'
    data = json.loads(requestPokemons)

    '''Ejecución del metodo a probar'''
    result = punto1.obtain_punto_1(data)

    '''Verificación'''
    assert result == 0

