import requests

def obtain_all_names_egg_groups(data):
    names = []
    for element in data['egg_groups']:
        type = element['url']
        dataEggGroups= requests.get(type)
        if dataEggGroups.status_code == 200:
            dataEggGroups = dataEggGroups.json()
            for elementSpecies in dataEggGroups['pokemon_species']:
                names.append(elementSpecies['name'])
        else:
            return "ERROR EN EL SERVICIO"
    return names

def obtain_number_species(names):
    cont = 0
    resultantList = []
    for element in names:
        if element not in resultantList:
            resultantList.append(element)
            cont = cont + 1
    print(resultantList)
    return cont