def obtain_punto_1(data):
    cont = 0
    for element in data['results']:
        name = element['name']
        if count_letters_at(name) & count_letters(name):
            cont = cont + 1
        elif count_letters_at(name):
            cont = cont + 1
        elif count_letters(name):
            cont = cont + 1
    return cont

def count_letters(name):
    if name.count("a") == 2:
        return True
    else:
        return False

def count_letters_at(name):
    if name.find("at") == -1:
        return False
    else:
        return True