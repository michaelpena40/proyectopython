import requests

def obtain_type_and_weigth(data, typePokemon):
    weight = 0
    for element in data['types']:
        type = element['type']
        if type['name'] == typePokemon:
            weight = data['weight']
    return weight

def obtain_numbers_weigth_pokemon_season_one(urlPokemon, pokemonType):
    weightsList = []
    for i in range(1, 152):
        dataGetPokemon = requests.get(f'{urlPokemon}{i}')
        if dataGetPokemon.status_code == 200:
            dataGetPokemon = dataGetPokemon.json()
            peso = obtain_type_and_weigth(dataGetPokemon, pokemonType)
            if peso != 0:
                weightsList.append(peso)
        else:
            return "ERROR EN EL SERVICIO"
    return weightsList