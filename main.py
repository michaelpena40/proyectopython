from ejercicios import punto1
from ejercicios import punto2
from ejercicios import punto3
import requests

def primer_punto():
    print("=================================== EJECUTANDO PRIMER EJERCICIO ==================================================")
    urlListPokemons = "https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0"
    data = requests.get(urlListPokemons)
    if data.status_code == 200:
        data = data.json()
        resultPoint1 = punto1.obtain_punto_1(data)
    print("RESULATDO DE POKEMON CON AT y DOS A ES IGUAL =", resultPoint1)

def segundo_punto():
    print("=================================== EJECUTANDO SEGUNDO EJERCICIO ==================================================")
    urlGetPokemonName = "https://pokeapi.co/api/v2/pokemon/"
    pokemon = "raichu"
    resultNames = []
    dataPokemon = requests.get(urlGetPokemonName+pokemon)
    if dataPokemon.status_code == 200:
        dataPokemon = dataPokemon.json()
        urlPokemonSpecies = dataPokemon['species']['url']
        dataPokemonSpecies = requests.get(urlPokemonSpecies)
        print(urlPokemonSpecies)
        if dataPokemonSpecies.status_code == 200:
            dataPokemonSpecies = dataPokemonSpecies.json()
            print(dataPokemonSpecies)
            resultNames = punto2.obtain_all_names_egg_groups(dataPokemonSpecies)
    print("ESPECIES DE POKEMON QUE PUEDEN PROCREAR CON " + pokemon + " = ", punto2.obtain_number_species(resultNames))

def tercer_punto():
    print("=================================== EJECUTANDO TERCER EJERCICIO ==================================================")
    urlGetPokemon = "https://pokeapi.co/api/v2/pokemon/"
    resultPoint3= []
    pokemonType ="fighting"
    weightsList = punto3.obtain_numbers_weigth_pokemon_season_one(urlGetPokemon,pokemonType)
    resultPoint3.append(max(weightsList))
    resultPoint3.append(min(weightsList))
    print(resultPoint3)

if __name__ == "__main__":
    primer_punto()
    segundo_punto()
    tercer_punto()